var express = require('express');
var router = express.Router();
const fs = require("fs");
const mustache = require("mustache");
/* GET home page. */
router.get('/', function(req, res, next) {
  const head = fs.readFileSync("./views/partials/head.mst").toString();
  const header = fs.readFileSync("./views/partials/header.mst").toString();
  const footer = fs.readFileSync("./views/partials/footer.mst").toString();
  const userTemplate = fs.readFileSync("./views/index.mst").toString();
 const Data={
   links:[{
      l:"",
      name:"Home"
   },
   {
     l:"/api/users",
     name: "Users"
   },
   {
     l:"/api/country",
     name:"Countries"
   },
  {
      l:"/api/about",
      name:"About"
  }
   ] 
 }
 const partials = {
  head: head,
  header: header,
  footer: footer,
  
};
res.send(mustache.render(userTemplate,Data,partials))
});

module.exports = router;