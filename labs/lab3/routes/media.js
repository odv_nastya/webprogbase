var express = require('express');
var router = express.Router();
const control=require('../controllers/media');
const Media = require('../models/media');

router.get('/:id',control.getMedia) ;
/* GET users listing. */
/**
 * TODO: get all users
 * @route GET /api/media/{id}
 * @group Media - media operations
 * @param {integer} id.path.required
 * @returns {Media.model} 200 - User object
 * @returns {Error} 404 - User not found
 */
router.post('/', control.postMedia) ;
/**
 * TODO: add media 
 * @route POST /api/media
 * @group Media - media operations
 * @consumes multipart/form-data
 * @param {file} media.formData.required -file with media
 * @returns {id.Media.model} 200 - id media object
 * @returns {Error} 400 - Bad request
 */
module.exports = router;