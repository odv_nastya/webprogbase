const router = require('express').Router();
const fs = require("fs");
const mustache = require("mustache");
const userRouter = require('./users');
const countryRouter=require('./country');
const mediaRouter=require('./media');

router.use('/users', userRouter);
router.use('/country', countryRouter);
router.use('/media', mediaRouter);
router.use('/about', function(req,res,next){
    const head = fs.readFileSync("./views/partials/head.mst").toString();
    const header = fs.readFileSync("./views/partials/header.mst").toString();
    const footer = fs.readFileSync("./views/partials/footer.mst").toString();
    const userTemplate = fs.readFileSync("./views/about.mst").toString();
   const Data={
     links:[{
        l:"/",
        name:"Home"
     },
     {
       l:"/api/users",
       name: "Users"
     },
     {
       l:"/api/country",
       name:"Countries"
     },
    {
        l:"",
        name:"About"
    }
     ] 
   }
   const partials = {
    head: head,
    header: header,
    footer: footer,
    
  };
  res.send(mustache.render(userTemplate,Data,partials));
});
module.exports = router;
