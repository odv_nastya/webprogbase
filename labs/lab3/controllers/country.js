// controllers/users.js
const CountryRepository = require('../repositories/countryRepository');
const reposit=require('../repositories/countryRepository')
const fs=require('fs');
const { strict } = require('assert');
const countryProperty = Symbol('country');
const mustache = require("mustache");
repository=new CountryRepository('./data/countries.json')
module.exports = {
    getCountries(req, res) {
        console.log(req.query.page,req.query.per_page,req.query.name,req.query.next,req.query.prev)
        const countries=repository.getCountries();
        let countries2= [];
        if(req.query.name){
            for (let i=0; i<countries.length;i++)
        {
            if(countries[i].nameCountry.includes(req.query.name))
            {
                console.log(countries[i]);
                countries2.push(countries[i]);
            }
            
        }
        }
        else countries2=countries;
        if(!req.query.page) req.query.page=1;
        let countries3=[];
        if(!('per_page' in req.query))req.query.per_page=3;
        if(req.query.next && (Math.ceil(countries2.length/req.query.per_page)!=req.query.page)) req.query.page++;
        if(req.query.prev&&(1!=req.query.page)) req.query.page--;
        console.log("i AM HERE")
        console.log(req.query.page);
        if(req.query.per_page>100)req.query.per_page=100;
        if (countries2) {
            if(('page' in req.query)&&('per_page' in req.query))
            {
                const f=req.query.per_page*(req.query.page-1);
                console.log(f);

                const lenght=Math.min(countries2.length,(req.query.page*req.query.per_page))
                console.log("gg")
                console.log((req.query.page*req.query.per_page));
                for (let i=f;i<lenght;i++)
                {
                    countries3.push(countries2[i]);
                }
                req[countryProperty] = countries3;
            }
            else countries3=countries2;
            const head = fs.readFileSync("./views/partials/head.mst").toString();
            const header = fs.readFileSync("./views/partials/header.mst").toString();
            const footer = fs.readFileSync("./views/partials/footer.mst").toString();
            const userTemplate = fs.readFileSync("./views/countries.mst").toString();
           const Data={
             links:[
             {
               l:"/",
               name: "Home"
             },
             {
                l:"/api/users",
                name:"Users"
             },
             {
               l:"",
               name:"Countries"
             },
             {
                 l:"/api/about",
                 name:"About"
             }
          
             ],
             page:
             {
                 name:req.query.name,
                 current: req.query.page,
                 number:Math.max(Math.ceil(countries2.length/req.query.per_page),1)

             }
           }
           const partials = {
            head: head,
            header: header,
            footer: footer,
            
          };
          Data.countries=countries3;
          res.send(mustache.render(userTemplate,Data,partials))
           // res.send(req[countryProperty]);
        } else {
            res.sendStatus(404);
        }

    },

    getCountryById(req, res) {
        const country = repository.getCountryById(parseInt(req.params.id));
        if (country) {
            req[countryProperty] = country;
            const head = fs.readFileSync("./views/partials/head.mst").toString();
            const header = fs.readFileSync("./views/partials/header.mst").toString();
            const footer = fs.readFileSync("./views/partials/footer.mst").toString();
            const userTemplate = fs.readFileSync("./views/country.mst").toString();
           const Data={
             links:[
             {
               l:"/",
               name: "Home"
             },
             {
                l:"/api/users",
                name:"Users"
             },
             {
               l:"/api/country",
               name:"Countries"
             },
             {
                 l:"/api/about",
                 name:"About"
             }
          
             ] 
           }
           const partials = {
            head: head,
            header: header,
            footer: footer,
            
          };
          Data.country=country;
          res.send(mustache.render(userTemplate,Data,partials))
            //res.send(req[countryProperty]);
        } else {
            res.sendStatus(404);
        }

    },
    getCountryHandler(req, res, next) {
        const country = repository.getCountryById(parseInt(req.params.id));
        if (country) {
            req[countryProperty] = country;
            next();
        } else {
            res.sendStatus(404);
        }

    },
    deleteCountryById(req, res){
        repository.deleteCountry(parseInt(req.params.id));
        res.send(req[countryProperty]);
    },
    addCountry(req,res){
        console.log("fghj");
        console.log(req.files);
        let country={};
        country.nameCountry=req.body.nameCountry;
        country.capital=req.body.capital;
        country.yearOfFound=req.body.datetime;
        country.population=req.body.population;
        country.area=req.body.area;
        console.log(req.files);
        let name=req.files['avaUrl'].name.split('.');
        let media={};
        media.filename='country'+repository.storage.nextId+'.'+name[1];
        fs.writeFileSync('./data/media/'+media.filename, req.files['avaUrl'].data);
        country.avaUrl='/media/'+media.filename;
        /*
        fs.writeFileSync('./newCountry/country.json',req.files['country'].data);
        const jsonText=fs.readFileSync('./newCountry/country.json');
        const jsonArray=JSON.parse(jsonText);*/

        if(!('nameCountry' in country)||!('capital'in country)||!('population' in country)||!('area' in country)||!('yearOfFound' in country)){
            res.sendStatus(400)
        }
        else{
            id=repository.addCountry(country);
            country.id=id;
            req[countryProperty]=country;
            res.redirect('/api/country/'+id);
        }

    },
    updateCountry(req,res){
        const country=repository.getCountryById(parseInt(req.params.id));
        fs.writeFileSync('./newCountry/country.json',req.files['country'].data);
        const jsonText=fs.readFileSync('./newCountry/country.json');
        const jsonArray=JSON.parse(jsonText);
        if('nameCountry' in jsonArray) country.nameCountry=jsonArray.nameCountry;
        if('capital' in jsonArray) country.capital=jsonArray.capital;
        if('population' in jsonArray) country.population=jsonArray.population;
        if('area' in jsonArray) country.area=jsonArray.area;
        if('yearOfFound' in jsonArray) country.yearOfFound=jsonArray.yearOfFound;
        repository.updateCountry(country);
        req[countryProperty]=country;
        res.send(req[countryProperty])
    },
    New(req,res)
    {
        console.log(req,res)
        const head = fs.readFileSync("./views/partials/head.mst").toString();
        const header = fs.readFileSync("./views/partials/header.mst").toString();
        const footer = fs.readFileSync("./views/partials/footer.mst").toString();
        const userTemplate = fs.readFileSync("./views/new.mst").toString();
       const Data={
         links:[{
            l:"/",
            name:"Home"
         },
         {
           l:"/api/users",
           name: "Users"
         },
         {
           l:"/api/country",
           name:"Countries"
         },
        {
            l:"/api/about",
            name:"About"
        }
         ] 
       }
       const partials = {
        head: head,
        header: header,
        footer: footer,
        
      };
      res.send(mustache.render(userTemplate,Data,partials))
    }
};

