const MediaRepository = require('../repositories/mediaRepository');
const reposit=require('../repositories/mediaRepository')

const userProperty = Symbol('user');
repo=new MediaRepository('./data/media.json');
const fs=require('fs');
module.exports = {
    getMedia(req, res) {
        const media=repo.getMediaById(parseInt(req.params.id));

        console.log(parseInt(req.params.id))
        if(media){
            let file=fs.readFileSync(__dirname+'/../data/media/'+media.file);
            res.send(Buffer.from(file));

        }
        else{
            res.sendStatus(404)
        }

    },

    postMedia(req, res) {
        console.log(req.files);
        let name=req.files['media'].name.split('.');
        let media={};
        media.file='country'+repo.storage.nextId+'.'+name[1];
        fs.writeFileSync('./data/media/'+media.file,req.files['media'].data);
        id=repo.postMedia(media);
        res.status(201).send(id.toString())
        
    },

};