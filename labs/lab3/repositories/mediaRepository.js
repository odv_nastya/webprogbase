// repositories/userRepository.js

const Media = require('../models/media');
const JsonStorage = require('../jsonStorage');
const media = require('../controllers/media');
 
class MediaRepository {
 
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }
 

    getMediaById(mediaId) {
        console.log('getUserById'.red);
        const items = this.storage.readItems();
        for(const item of items)
        {
            if(item.id===mediaId)
            {
                return new Media(item.id, item.file);
            }
        }
        return null;
    }
    postMedia(media){
        let medias;
        medias=this.storage.readItems();
        media.id=this.storage.nextId;
        medias.push(media);
        this.storage.writeItems(medias);
        return media.id;
    }
    
};
module.exports = MediaRepository;