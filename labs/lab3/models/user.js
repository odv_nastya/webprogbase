/**
 * @typedef User
 * @property {integer} id.required
 * @property {string} login.required - unique username
 * @property {string} fullname 
 * @property {number} role.required -admin or not
 * @property {string} registeredAt.required -check-in time
 * @property {string} avaUrl - ava
 * @property {number} isEnable.required - loced or not
 * @property {string} bio - bio
 */







class User {

    constructor(id, login, fullname,role,registeredAt,avaUrl,isEnable,bio) {
        this.id = id;  // number
        this.login = login;  // string
        this.fullname = fullname;  // string
        this.role=role;
        this.registeredAt=registeredAt;
        this.avaUrl=avaUrl;
        this.isEnable=isEnable;
        this.bio=bio;
        // TODO: More fields
    }
 };
 
 module.exports = User;
 