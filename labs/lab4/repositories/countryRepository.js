const Country = require('../models/country');
const JsonStorage = require('../jsonStorage');
const e = require('express');
const fs=require('fs').promises;
const mongoose = require('mongoose');
const CountryModel=require('../shema/country');
class CountryRepository {
 
    constructor(dbUrl) {
      
        this.storage = new JsonStorage(dbUrl,CountryModel);

    }
 
    async getCountries() { 
        await this.storage.connect();
        const result=await this.storage.getItems();
        await this.storage.disconect();
        console.log(result);
        return result;
    }
 
    async getCountryById(countryId) {
        console.log("TYTA");
        console.log(countryId);
        await this.storage.connect();
        const result = await this.storage.getItembyId(countryId);
        console.log(result);
        await this.storage.disconect();
        return result;
    }
    async addCountry(entityModel){
        await this.storage.connect();
        const id=await this.storage.addItem(entityModel);
        await this.storage.disconect();
        return id;
    }
    async updateCountry(entityModel){
        await this.storage.connect();
        await this.storage.updateItem(entityModel);
        await this.storage.disconect();
        
    }
    async deleteCountry(entityId){
        await this.storage.connect();
        const pic=await this.storage.getItembyId(entityId);
        await this.storage.disconect();
        console.log("KARTINKA");
        console.log(pic.avaUrl);
        await this.storage.connect();
        await this.storage.deleteItem(entityId);
        await fs.unlink('./data'+pic.avaUrl);
        await this.storage.disconect();
    }
 
    
};
module.exports = CountryRepository;
 