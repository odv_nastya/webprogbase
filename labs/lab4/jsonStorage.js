// jsonStorage.js
const fs = require('fs').promises;
const mongoose = require('mongoose');
const dbUrl = 'mongodb://localhost:27017/webdb';
    const connectOptions = {
   useNewUrlParser: true,
   useUnifiedTopology: true,
    };
class JsonStorage {

    // filePath - path to JSON file
    constructor(dbUrl, model) {
        this.dbUrl=dbUrl;
        this.model=model;
    }
    async connect(){

        console.log(`Server ready`);
        this.client = await mongoose.connect(this.dbUrl, connectOptions);
        console.log('Mongo database connected');
    }

    async getItems() {
        const userDocs = await this.model.find().populate("id_continent").populate("id_user");
        console.log(userDocs);
        return userDocs;

    }
 
    async addItem(items) {
        const userDoc = await this.model(items).save();
        return userDoc.id;
    }
    async getItembyId(id){
        const userDocs = await this.model.findById(id).populate("id_continent").populate("id_user");
        console.log("jsonStorage TYTA");
        console.log(id);
        console.log(userDocs);
        return userDocs;
    }
    async deleteItem(id){

        await this.model.findByIdAndRemove(id);
    }
    async updateItem(item){
        await this.model.findByIdAndUpdate(item.id,item)
    }
    async disconect(){
        this.client.disconnect();
        console.log('Mongo database disconnected');
    }
};
 
module.exports = JsonStorage;