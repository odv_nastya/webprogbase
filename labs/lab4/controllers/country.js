// controllers/users.js
const CountryRepository = require('../repositories/countryRepository');
const UserRepository = require('../repositories/userRepository');
const ContinentRepository = require('../repositories/continentRepository');
const mediaRepository = require('../repositories/mediaRepository');
const reposit=require('../repositories/countryRepository')
const fs=require('fs').promises;
const fs2=require('fs');
const { strict } = require('assert');
const countryProperty = Symbol('country');
const mustache = require("mustache");
const config = require('./config');
 
 const { db: { host, port, name } } = config.dev;
 const connectionString = `mongodb+srv://${host}:${port}/${name}?retryWrites=true&w=majority`;
//console.log(connectionString);

reposito=new CountryRepository(connectionString)
reposito2=new ContinentRepository(connectionString)
reposito3=new UserRepository(connectionString)
reposito4=new mediaRepository()
module.exports = {
    async getCountries(req, res, next) {
        try{
            let a="I want to die";
            console.log(a);
        console.log(req.query.page,req.query.per_page,req.query.name,req.query.next,req.query.prev)
        const countries=await reposito.getCountries();
        let countries2= [];
        if(req.query.name){
            for (let i=0; i<countries.length;i++)
        {
            if(countries[i].nameCountry.includes(req.query.name))
            {
                console.log(countries[i]);
                countries2.push(countries[i]);
            }
            
        }
        }
        else countries2=countries;
        if(!req.query.page) req.query.page=1;
        let countries3=[];
        if(!('per_page' in req.query))req.query.per_page=3;
        if(req.query.next && (Math.ceil(countries2.length/req.query.per_page)!=req.query.page)) req.query.page++;
        if(req.query.prev&&(1!=req.query.page)) req.query.page--;
        console.log("i AM HERE")
        console.log(req.query.page);
        if(req.query.per_page>100)req.query.per_page=100;
        if (countries2) {
            if(('page' in req.query)&&('per_page' in req.query))
            {
                const f=req.query.per_page*(req.query.page-1);
                console.log(f);

                const lenght=Math.min(countries2.length,(req.query.page*req.query.per_page))
                console.log("gg")
                console.log((req.query.page*req.query.per_page));
                for (let i=f;i<lenght;i++)
                {
                    countries3.push(countries2[i]);
                }
                req[countryProperty] = countries3;
            }
            else countries3=countries2;
            const result=await Promise.all([
                fs.readFile("./views/partials/head.mst"),
                fs.readFile("./views/partials/header.mst"),
                fs.readFile("./views/partials/footer.mst"),
                fs.readFile("./views/countries.mst")
            ])
            const head = result[0].toString();
            const header =result[1].toString();
            const footer =result[2].toString();
            const userTemplate =result[3].toString();
           const Data={
             links:[
             {
               l:"/",
               name: "Home"
             },
             {
                l:"/api/users",
                name:"Users"
             },
             {
               l:"",
               name:"Countries"
             },
             {
                l:"/api/continent",
                name:"Continents"
            },
             {
                 l:"/api/about",
                 name:"About"
             }
          
             ],
             page:
             {
                 name:req.query.name,
                 current: req.query.page,
                 number:Math.max(Math.ceil(countries2.length/req.query.per_page),1)

             }
           }
           const partials = {
            head: head,
            header: header,
            footer: footer,
            
          };
          Data.countries=countries3;
          res.send(mustache.render(userTemplate,Data,partials))
        } else {
            // hello
            res.sendStatus(404);
        }
    }
    catch(err)
    {
        return await next(err);
    }

    },

    async getCountryById(req, res, next) {
    try{
        const country =await reposito.getCountryById(req.params.id);
        if (country) {
            req[countryProperty] = country;
            const result=await Promise.all([
                fs.readFile("./views/partials/head.mst"),
                fs.readFile("./views/partials/header.mst"),
                 fs.readFile("./views/partials/footer.mst"),
                fs.readFile("./views/country.mst")
            ])
            const head =result[0].toString();
            const header = result[1].toString();
            const footer = result[2].toString();
            const userTemplate = result[3].toString();
           const Data={
             links:[
             {
               l:"/",
               name: "Home"
             },
             {
                l:"/api/users",
                name:"Users"
             },
             {
               l:"/api/country",
               name:"Countries"
             },
             {
                l:"/api/continent",
                name:"Continents"
              },
             {
                 l:"/api/about",
                 name:"About"
             }
          
             ] 
           }
           const partials = {
            head: head,
            header: header,
            footer: footer,
            
          };
          Data.country=country;
          res.send(mustache.render(userTemplate,Data,partials))
            //res.send(req[countryProperty]);
        } else {
            res.sendStatus(404);
        }
    }
    catch(err)
    {
        return await next(err);
    }
    },
    async getCountryHandler(req, res, next) {
        try{

        
        const country =await reposito.getCountryById(req.params.id);
        if (country) {
            req[countryProperty] = country;
            next();
        } else {
            res.sendStatus(404);
        }
    }
    catch(err)
    {
        return await next(err);
    }

    },
    async deleteCountryById(req, res, next){
        try{
        await reposito.deleteCountry(req.params.id);
        res.send(req[countryProperty]);
        }
        catch(err){
            return await next(err);
        }
    },
    async addCountry(req,res,next){
        try{
        console.log("fghj");
        console.log(req.files);
        let country={};
        country.nameCountry=req.body.nameCountry;
        country.id_continent=req.body.continent;
        country.id_user=req.body.user;
        country.capital=req.body.capital;
        country.yearOfFound=req.body.datetime;
        country.population=req.body.population;
        country.area=req.body.area;
        console.log(req.files);
        
        const o = await reposito4.uploadRaw(req.files['avaUrl'].data);
        console.log(req.files)



        country.avaUrl=o.url;
        /*
        const jsonArray=JSON.parse(jsonText);*/

        if(!('nameCountry' in country)||!('capital'in country)||!('population' in country)||!('area' in country)||!('yearOfFound' in country)){
            res.sendStatus(400)
        }
        else{
            id=await reposito.addCountry(country);
            country.id=id;
            req[countryProperty]=country;
            res.redirect('/api/country/'+id);
        }
    }
    catch(err)
    {
        return await next(err);
    }

    },
    async updateCountry(req,res,next){
        try{
        const country=await reposito.getCountryById(req.params.id);
        await fs.writeFileSync('./newCountry/country.json',req.files['country'].data);
        const jsonText=await fs.readFile('./newCountry/country.json');
        const jsonArray=JSON.parse(jsonText);
        if('nameCountry' in jsonArray) country.nameCountry=jsonArray.nameCountry;
        if('capital' in jsonArray) country.capital=jsonArray.capital;
        if('population' in jsonArray) country.population=jsonArray.population;
        if('area' in jsonArray) country.area=jsonArray.area;
        if('yearOfFound' in jsonArray) country.yearOfFound=jsonArray.yearOfFound;
        await reposito.updateCountry(country);
        req[countryProperty]=country;
        res.send(req[countryProperty])
        }
        catch(err)
        {
            return await next(err);
        }

    },
    async New(req,res,next)
    {
        try{
            const continents = await reposito2.getContinents();
            const users = await reposito3.getUsers();
        console.log(req,res)
        const result =await Promise.all([
            fs.readFile("./views/partials/head.mst"),
            fs.readFile("./views/partials/header.mst"),
            fs.readFile("./views/partials/footer.mst"),
            fs.readFile("./views/new.mst")
        ])
        const head = result[0].toString();
        const header = result[1].toString();
        const footer = result[2].toString();
        const userTemplate = result[3].toString();
       const Data={
         links:[{
            l:"/",
            name:"Home"
         },
         {
           l:"/api/users",
           name: "Users"
         },
         {
           l:"/api/country",
           name:"Countries"
         },
         {
            l:"/api/continent",
            name:"Continents"
          },
        {
            l:"/api/about",
            name:"About"
        }
         ] 
       }
       Data.continents=continents;

       Data.users=users;
       console.log(continents);
       console.log(users);
       const partials = {
        head: head,
        header: header,
        footer: footer,
        
      };
      res.send(mustache.render(userTemplate,Data,partials))
    }
    catch(err){
        return await next(err);
    }
    },
    async anyErorr(err, req, res, next){
        console.error(err.stack)
        res.status(500).send('Something broke!') 
      }
};

