// controllers/users.js
const ContinentRepository = require('../repositories/continentRepository');
const CountryRepository = require('../repositories/countryRepository');
const reposit=require('../repositories/continentRepository')
const mediaRepository = require('../repositories/mediaRepository');
const fs=require('fs').promises;
const fs2=require('fs');
const { strict } = require('assert');
const countryProperty = Symbol('continent');
const mustache = require("mustache");
const config = require('./config');

 const { db: { host, port, name } } = config.dev;
 const connectionString = `mongodb+srv://${host}:${port}/${name}?retryWrites=true&w=majority`;
console.log(connectionString);
repository=new ContinentRepository(connectionString)
repository2=new CountryRepository(connectionString)
repository3=new mediaRepository()
module.exports = {
    async getContinent(req, res, next) {
        try{

        console.log(req.query.page,req.query.per_page,req.query.name,req.query.next,req.query.prev)
        const countries=await repository.getContinents();
        let countries2= [];
        if(req.query.name){
            for (let i=0; i<countries.length;i++)
        {
            if(countries[i].nameCountry.includes(req.query.name))
            {
                console.log(countries[i]);
                countries2.push(countries[i]);
            }
            
        }
        }
        else countries2=countries;
        if(!req.query.page) req.query.page=1;
        let countries3=[];
        if(!('per_page' in req.query))req.query.per_page=3;
        if(req.query.next && (Math.ceil(countries2.length/req.query.per_page)!=req.query.page)) req.query.page++;
        if(req.query.prev&&(1!=req.query.page)) req.query.page--;
        console.log("i AM HERE")
        console.log(req.query.page);
        if(req.query.per_page>100)req.query.per_page=100;
        if (countries2) {
            if(('page' in req.query)&&('per_page' in req.query))
            {
                const f=req.query.per_page*(req.query.page-1);
                console.log(f);

                const lenght=Math.min(countries2.length,(req.query.page*req.query.per_page))
                console.log("gg")
                console.log((req.query.page*req.query.per_page));
                for (let i=f;i<lenght;i++)
                {
                    countries3.push(countries2[i]);
                }
                req[countryProperty] = countries3;
            }
            else countries3=countries2;
            const result=await Promise.all([
                fs.readFile("./views/partials/head.mst"),
                fs.readFile("./views/partials/header.mst"),
                fs.readFile("./views/partials/footer.mst"),
                fs.readFile("./views/continents.mst")
            ])
            const head = result[0].toString();
            const header =result[1].toString();
            const footer =result[2].toString();
            const userTemplate =result[3].toString();
           const Data={
             links:[
             {
               l:"/",
               name: "Home"
             },
             {
                l:"/api/users",
                name:"Users"
             },
             {
               l:"/api/country",
               name:"Countries"
             },
             {
                l:"",
                name:"Continents"
            },
             {
                 l:"/api/about",
                 name:"About"
             }
          
             ],
             page:
             {
                 name:req.query.name,
                 current: req.query.page,
                 number:Math.max(Math.ceil(countries2.length/req.query.per_page),1)

             }
           }
           const partials = {
            head: head,
            header: header,
            footer: footer,
            
          };
          Data.continents=countries3;
          console.log("Tyta ya");
          console.log(countries3);
          res.send(mustache.render(userTemplate,Data,partials))
           // res.send(req[countryProperty]);
        } else {
            res.sendStatus(404);
        }
    }
    catch(err)
    {
        return await next(err);
    }

    },

    async getContinentById(req, res, next) {
    try{
        const country =await repository.getContinentById(req.params.id);
        if (country) {
            req[countryProperty] = country;
            const result=await Promise.all([
                fs.readFile("./views/partials/head.mst"),
                fs.readFile("./views/partials/header.mst"),
                 fs.readFile("./views/partials/footer.mst"),
                fs.readFile("./views/continent.mst")
            ])
            const head =result[0].toString();
            const header = result[1].toString();
            const footer = result[2].toString();
            const userTemplate = result[3].toString();
           const Data={
             links:[
             {
               l:"/",
               name: "Home"
             },
             {
                l:"/api/users",
                name:"Users"
             },
             {
               l:"/api/country",
               name:"Countries"
             },
             {
                l:"/api/continent",
                name:"Countinents"
              },
             {
                 l:"/api/about",
                 name:"About"
             }
          
             ] 
           }
           const partials = {
            head: head,
            header: header,
            footer: footer,
            
          };
          Data.continent=country;
          res.send(mustache.render(userTemplate,Data,partials))
            //res.send(req[countryProperty]);
        } else {
            res.sendStatus(404);
        }
    }
    catch(err)
    {
        return await next(err);
    }
    },
    async getContinentHandler(req, res, next) {
        try{

        
        const country =await repository.getContinentById(req.params.id);
        if (country) {
            req[countryProperty] = country;
            next();
        } else {
            res.sendStatus(404);
        }
    }
    catch(err)
    {
        return await next(err);
    }

    },
    async deleteContinentById(req, res, next){
        try{
            const countries=await repository2.getCountries();
            console.log("qwerty TYTA");
            console.log(countries);
            let countries2=[];
            for(let i=0;i<countries.length;i++){
                console.log("ID!!!");
                console.log(countries[i].id_continent);
                if(countries[i].id_continent._id==req.params.id){
                    countries2.push(countries[i]);
                }
            }
            console.log("qwerty TYTA2");
            console.log(countries2);
            for(let i=0;i<countries2.length;i++){
                await repository2.deleteCountry(countries2[i]._id);
            }
        await repository.deleteContinent(req.params.id);
        res.send(req[countryProperty]);
        }
        catch(err){
            return await next(err);
        }
    },
    async addContinent(req,res,next){
        try{
        console.log("fghj");
        console.log(req.files);
        let country={};
        country.nameContinent=req.body.nameContinent;
        country.quantityOfCountries=req.body.quantityOfCountries;
        country.ocean=req.body.ocean;
        country.numberOfTimeZone=req.body.numberOfTimeZone;
        country.area=req.body.area;
        console.log(req.files);
        // let name=req.files['avaUrl'].name.split('.');
        // let media={};
        //(await repository.storage.nextId())
        // let name2=name[0];
        // let z=2;
        // while( fs2.existsSync('./data/media/'+ name2 +'.'+name[1])){
        //     name2=name[0]+z;
        //     z++;
        // }
        // media.filename=name2+'.'+name[1];
        // await fs.writeFile('./data/media/'+media.filename, req.files['avaUrl'].data);
        
        const o = await repository3.uploadRaw(req.files['avaUrl'].data);
        country.avaUrl=o.url;
        /*
        fs.writeFileSync('./newCountry/country.json',req.files['country'].data);
        const jsonText=fs.readFileSync('./newCountry/country.json');
        const jsonArray=JSON.parse(jsonText);*/

        if(!('nameContinent' in country)||!('quantityOfCountries'in country)||!('ocean' in country)||!('area' in country)||!('numberOfTimeZone' in country)){
            res.sendStatus(400)
        }
        else{
            id=await repository.addContinent(country);
            country.id=id;
            req[countryProperty]=country;
            res.redirect('/api/continent/'+id);
        }
    }
    catch(err)
    {
        return await next(err);
    }

    },
    async updateContinent(req,res,next){
        try{
        const country=await repository.getCountryById(req.params.id);
        await fs.writeFileSync('./newCountry/country.json',req.files['country'].data);
        const jsonText=await fs.readFile('./newCountry/country.json');
        const jsonArray=JSON.parse(jsonText);
        if('nameCountry' in jsonArray) country.nameCountry=jsonArray.nameCountry;
        if('capital' in jsonArray) country.capital=jsonArray.capital;
        if('population' in jsonArray) country.population=jsonArray.population;
        if('area' in jsonArray) country.area=jsonArray.area;
        if('yearOfFound' in jsonArray) country.yearOfFound=jsonArray.yearOfFound;
        await repository.updateCountry(country);
        req[countryProperty]=country;
        res.send(req[countryProperty])
        }
        catch(err)
        {
            return await next(err);
        }

    },
    async New(req,res,next)
    {
        try{

        console.log(req,res)
        const result =await Promise.all([
            fs.readFile("./views/partials/head.mst"),
            fs.readFile("./views/partials/header.mst"),
            fs.readFile("./views/partials/footer.mst"),
            fs.readFile("./views/new2.mst")
        ])
        const head = result[0].toString();
        const header = result[1].toString();
        const footer = result[2].toString();
        const userTemplate = result[3].toString();
       const Data={
         links:[{
            l:"/",
            name:"Home"
         },
         {
           l:"/api/users",
           name: "Users"
         },
         {
           l:"/api/country",
           name:"Countries"
         },
         {
            l:"/api/continent",
            name:"Countinents"
          },
        {
            l:"/api/about",
            name:"About"
        }
         ] 
       }
       const partials = {
        head: head,
        header: header,
        footer: footer,
        
      };
      res.send(mustache.render(userTemplate,Data,partials))
    }
    catch(err){
        return await next(err);
    }
    },
    async anyErorr(err, req, res, next){
        console.error(err.stack)
        res.status(500).send('Something broke!') 
      }
};

