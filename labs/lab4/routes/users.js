var express = require('express');
var router = express.Router();
const control=require('../controllers/users');

/* GET users listing. */
/**
 * TODO: get all users
 * @route GET /api/users
 * @group Users - user operations
 * @param {integer} page.query -page number
 * @param  {integer} per_page.query - items per page
 * @returns {User.model} 200 - User object
 * @returns {Error} 404 - User not found
 */
router.get('/', control.getUsers,control.anyErorr) ;
/**
 * TODO: get user by id
 * @route GET /api/users/{id}
 * @group Users - user operations
 * @param {integer} id.path.required - id of the User - eg: 1
 * @returns {User.model} 200 - User object
 * @returns {Error} 404 - User not found
 */

router.get('/:id',control.getUserById,control.anyErorr) ;
module.exports = router;