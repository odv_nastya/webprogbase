const mongoose=require('mongoose');
const userSchema = new mongoose.Schema({
    login: { type: String, required: true },
    role: { type: Number, required: true },
    fullname: { type: String, required: true },
    registeredAt: { type: Date, required: true },
    avaUrl: { type: String, required: true },
    bio: { type: String, required: true},
    isEnabled: { type: Boolean, required: true}
 });
 const userModel = mongoose.model('User', userSchema);
 module.exports=userModel;