// controllers/users.js
const UserRepository = require('../repositories/userRepository');
//const reposit=require('../repositories/userRepository')

const userProperty = Symbol('user');
reposit=new UserRepository('./data/users.json')
module.exports = {
    getUsers(req, res) {
        console.log(req.query.page,req.query.per_page)
        if(!('per_page' in req.query))req.query.per_page=10
        if(req.query.per_page>100)req.query.per_page=100
        const users = reposit.getUsers();
        if (users) {
            if(('page' in req.query)&&('per_page' in req.query))
            {
                const f=req.query.per_page*(req.query.page-1);
                console.log(f);
                let user=[];
                const lenght=Math.min(users.length,(req.query.page*req.query.per_page))
                console.log(lenght);
                for (let i=f;i<lenght;i++)
                {
                    user.push(users[i]);
                }
                req[userProperty] = user;
            }
            else req[userProperty] = users;
            
            res.send(req[userProperty]);
        } else {
            res.sendStatus(404);
        }

    },

    getUserById(req, res) {
        const user = reposit.getUserById(parseInt(req.params.id));
        if (user) {
            req[userProperty] = user;
            res.send(req[userProperty]);
        } else {
            res.sendStatus(404);
        }

    },

};

