// controllers/users.js
const CountryRepository = require('../repositories/countryRepository');
const reposit=require('../repositories/countryRepository')
const fs=require('fs');
const { strict } = require('assert');
const countryProperty = Symbol('country');
repository=new CountryRepository('./data/countries.json')
module.exports = {
    getCountries(req, res) {
        console.log(req.query.page,req.query.per_page)
        if(!('per_page' in req.query))req.query.per_page=10
        if(req.query.per_page>100)req.query.per_page=100

        const countries = repository.getCountries();
        console.log(countries);
        if (countries) {
            if(('page' in req.query)&&('per_page' in req.query))
            {
                const f=req.query.per_page*(req.query.page-1);
                console.log(f);
                let country=[];
                const lenght=Math.min(countries.length,(req.query.page*req.query.per_page))
                console.log(lenght);
                for (let i=f;i<lenght;i++)
                {
                    country.push(countries[i]);
                }
                req[countryProperty] = country;
            }
            else req[countryProperty] = countries;
            
            res.send(req[countryProperty]);
        } else {
            res.sendStatus(404);
        }

    },

    getCountryById(req, res) {
        const country = repository.getCountryById(parseInt(req.params.id));
        if (country) {
            req[countryProperty] = country;
            res.send(req[countryProperty]);
        } else {
            res.sendStatus(404);
        }

    },
    getCountryHandler(req, res, next) {
        const country = repository.getCountryById(parseInt(req.params.id));
        if (country) {
            req[countryProperty] = country;
            next();
        } else {
            res.sendStatus(404);
        }

    },
    deleteCountryById(req, res){
        repository.deleteCountry(parseInt(req.params.id));
        res.send(req[countryProperty]);
    },
    addCountry(req,res){
        console.log("fghj");
        console.log(req.files);
        fs.writeFileSync('./newCountry/country.json',req.files['country'].data);
        const jsonText=fs.readFileSync('./newCountry/country.json');
        const jsonArray=JSON.parse(jsonText);
        if(!('nameCountry' in jsonArray)||!('capital'in jsonArray)||!('population' in jsonArray)||!('area' in jsonArray)||!('yearOfFound' in jsonArray)){
            res.sendStatus(400)
        }
        else{
            id=repository.addCountry(jsonArray);
            jsonArray.id=id;
            req[countryProperty]=jsonArray;
            res.statusCode=201;
            //res.setHeader("location",)
            res.send(req[countryProperty]);
        }

    },
    updateCountry(req,res){
        const country=repository.getCountryById(parseInt(req.params.id));
        fs.writeFileSync('./newCountry/country.json',req.files['country'].data);
        const jsonText=fs.readFileSync('./newCountry/country.json');
        const jsonArray=JSON.parse(jsonText);
        if('nameCountry' in jsonArray) country.nameCountry=jsonArray.nameCountry;
        if('capital' in jsonArray) country.capital=jsonArray.capital;
        if('population' in jsonArray) country.population=jsonArray.population;
        if('area' in jsonArray) country.area=jsonArray.area;
        if('yearOfFound' in jsonArray) country.yearOfFound=jsonArray.yearOfFound;
        repository.updateCountry(country);
        req[countryProperty]=country;
        res.send(req[countryProperty])
    }
};

