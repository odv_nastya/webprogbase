/**
 * @typedef Media
 * @property {integer} id.required
 * @property {string} name.required - name of the country
 */

class Media {

    constructor(id, file) {
        this.id = id;  // number
        this.file=file;
        // TODO: More fields
    }
 };
 module.exports = Media;