// jsonStorage.js
const fs = require('fs');
class JsonStorage {
 
    // filePath - path to JSON file
    constructor(filePath) {
        this.filePath=filePath;
    }
 
    get nextId() {
        const jsonText=fs.readFileSync(this.filePath);
        const jsonArray=JSON.parse(jsonText);
        return jsonArray.nextId;
    }
 
    incrementNextId() {
        return (this.nextId +1);
    }
 
    readItems() {
        const jsonText = fs.readFileSync(this.filePath);
        const jsonArray = JSON.parse(jsonText);
        return jsonArray.items;

    }
 
    writeItems(items) {
        let newId =this.nextId;
        if(items.length>this.readItems().length){
            newId=this.incrementNextId();
        }
        const obj={
            nextId : newId,
            items : items,
        };
    fs.writeFileSync(this.filePath,JSON.stringify(obj, null, 4));
    }
};
 
module.exports = JsonStorage;