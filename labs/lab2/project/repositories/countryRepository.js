const Country = require('../models/country');
const JsonStorage = require('../jsonStorage');
class CountryRepository {
 
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }
 
    getCountries() { 
        const items = this.storage.readItems();
        let countriesArr = [];
        for (const item of items)
        {
            countriesArr.push(new Country(item.id, item.nameCountry, item.capital,item.population, item.area, item.yearOfFound));
        }
        return countriesArr;
    }
 
    getCountryById(countryId) {
        //console.log('getUserById'.red);
        const countries = this.getCountries();
        for(const country of countries)
        {
            if(country.id===countryId)
            {
                return new Country (country.id, country.nameCountry, country.capital,country.population, country.area, country.yearOfFound);
            }
        }
        return null;
    }
    addCountry(entityModel){
        let Countries;
        Countries=this.storage.readItems();
        entityModel.id=this.storage.nextId;
        Countries.push(entityModel);
        this.storage.writeItems(Countries);
        return entityModel.id;
    }
    updateCountry(entityModel){
        let Countries;
        Countries=this.storage.readItems();
        for(const item of Countries){
            if(item.id===entityModel.id){
                item.nameCountry=entityModel.nameCountry;
                item.capital=entityModel.capital;
                item.population=entityModel.population;
                item.area=entityModel.area;
                item.yearOfFound=entityModel.yearOfFound;
                this.storage.writeItems(Countries);
                return true;
            }
        }
        return false;
    }
    deleteCountry(entityId){
        let Countries;
        
        Countries=this.storage.readItems();
        for(let i=0;i<Countries.length;i++){
            if(Countries[i].id===entityId){
                Countries.splice(i,1);
                this.storage.writeItems(Countries);
                return true;
            }
        }
        return false;
    }
 
    
};
module.exports = CountryRepository;
 