
var express = require('express');
var router = express.Router();
const control=require('../controllers/country');
const Country = require('../models/country');
/**
 * TODO: get countries
 * @route GET /api/country
 * @group Countries -  operations
 * @param {integer} page.query -page number
 * @param  {integer} per_page.query - items per page
 * @returns {Array.<Country>} 200 - all countries
 * @returns {Error} 404 - 
 */

router.get('/', control.getCountries) ;
router.get('/:id',control.getCountryById) ;
/**
 * TODO: get country by id
 * @route GET /api/country/{id}
 * @group Countries -  operations
 * @param {integer} id.path.required - id of the Country- eg: 1
 * @returns {Country.model} 200 - Country object
 * @returns {Error} 404 - Country not found
 */


router.put("/:id",control.getCountryHandler,control.updateCountry);
/**
 * TODO: update country by id
 * @route PUT /api/country/{id}
 * @group Countries - user operations
 * @param {integer} id.path.required - id of the Country - eg: 1
 * @consumes multipart/form-data
 * @param {file} country.formData.required -file with country
 * @returns {Country.model} 200 -changed Country object
 * @returns {Error} 404 - Country not found
 */

router.post('/',control.addCountry);
/**
 * TODO: add country 
 * @route POST /api/country
 * @group Countries -  operations
 * @consumes multipart/form-data
 * @param {file} country.formData.required -file with country
 * @returns {Country.model} 200 - Country object
 * @returns {Error} 400 - Bad request
 */

router.delete("/:id", control.getCountryHandler,control.deleteCountryById)
/**
 * TODO: delete country by id
 * @route DELETE /api/country/{id}
 * @group Countries -  operations
 * @param {integer} id.path.required - id of the User - eg: 1
 * @returns {Country.model} 200 - delete Country object
 * @returns {Error} 404 - Not found
 */
module.exports = router;