const router = require('express').Router();

const userRouter = require('./users');
const countryRouter=require('./country');
const mediaRouter=require('./media');
router.use('/users', userRouter);
router.use('/country', countryRouter);
router.use('/media', mediaRouter);
module.exports = router;
