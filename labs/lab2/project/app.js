var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var app = express();
var indexRouter = require('./routes/index');
var apiRouter = require('./routes/api');
const expressSwaggerGenerator = require('express-swagger-generator');
const expressSwagger = expressSwaggerGenerator(app);
const options = {
  swaggerDefinition: {
      info: {
          description: 'GET for user and country, POST,PUT,DELETE for country',
          title: 'Country',
          version: '1.0.0',
      },
      host: 'localhost:3000',
      produces: [ "application/json" ],
  },
  basedir: __dirname,
  files: ['./routes/**/*.js', './models/**/*.js'],
};
expressSwagger(options);

const busboy = require('busboy-body-parser');
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
const options2 = {
  limit: '5mb',
  multi: false,
};
app.use(busboy(options2));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/api', apiRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});





module.exports = app;
