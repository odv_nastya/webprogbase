class Country {

    constructor(id, nameCountry, capital,population,area,yearOfFound) {
        this.id = id;  // number
        this.nameCountry = nameCountry;  // string
        this.capital = capital;  // string
        this.population=population;
        this.area=area;
        this.yearOfFound=yearOfFound;
        
        // TODO: More fields
    }
 };
 
 module.exports = Country;