class User {

    constructor(id, login, fullname,role,registeredAt,avaUrl,isEnable) {
        this.id = id;  // number
        this.login = login;  // string
        this.fullname = fullname;  // string
        this.role=role;
        this.registeredAt=registeredAt;
        this.avaUrl=avaUrl;
        this.isEnable=isEnable;
        // TODO: More fields
    }
 };
 
 module.exports = User;
 