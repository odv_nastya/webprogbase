// repositories/userRepository.js

const User = require('../models/user');
const JsonStorage = require('../jsonStorage');
 
class UserRepository {
 
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }
 
    getUsers() { 
        const items = this.storage.readItems();
        let usersArr = [];
        for (const item of items)
        {
            usersArr.push(new User(item.id, item.login, item.fullname, item.role, item.registeredAt, item.avaUrl, item.isEnabled));
        }
        return usersArr;
    }
 
    getUserById(userId) {
        console.log('getUserById'.red);
        const users = this.getUsers();
        for(const user of users)
        {
            if(user.id===userId)
            {
                return new User (user.id, user.login, user.fullname, user.role, user.registeredAt, user.avaUrl, user.isEnabled);
            }
        }
        return null;
    }
 
    
};
module.exports = UserRepository;
