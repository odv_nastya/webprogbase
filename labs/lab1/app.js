// app.js
const readlineSync=require('readline-sync');
const UserRepository = require('./repositories/userRepository');
const JsonStorage = require('./jsonStorage');
const CountryRepository = require('./repositories/countryRepository');
const userRepository = new UserRepository("data/users.json");
const countryRepository = new CountryRepository("data/countries.json");
const userStorage = new JsonStorage('./data/users.json');
const User = require('./models/user');
const Country=require('./models/country');

    while(true)
    {
        const command = readlineSync.question("Enter command: ");
        
    if (command === "get/user") {
         // TODO: implement get/user
        let items=[];
        items=userRepository.getUsers();
        for(const item of items)
        {
            console.log(`User: ${item.login} "${item.fullname}"`);
        }
        
        
    } else if (command.substring(0,9) === `get/user/`) {
        
        const userId =Number(command.substring(9,command.lenght));
        if(isNaN(userId))
        {
            console.log("Please insert the number after get/user/");
            command;
        }else{
        const user = userRepository.getUserById(userId);
        if (!user) {
            console.log(`Error: user with id ${userId} not found.`);
            command;
        }else{
        console.log(`User: ${user.login} "${user.fullname}"`);}}
    } else  if (command === "get/country") {
        // TODO: implement get/user
       let items=[];
       items=countryRepository.getCountries();
       for(const item of items)
       {
           console.log(`Country: ${item.nameCountry} 
           capital:${item.capital}`);
       }
   }
   else if (command.substring(0,12) === `get/country/`) {
        const countryId =Number(command.substring(12,command.lenght));
        if(isNaN(countryId))
        {
            console.log("Please insert the number after get/country/");
            command;
        }
        else{
        const country = countryRepository.getCountryById(countryId);
        if (!country) {
            console.log(`Error: country with id ${countryId} not found.`);
            command;
        }else{
        console.log(`Country: ${country.nameCountry} 
        capital:${country.capital}
        population:${country.population}
        area:${country.area}`);}
    }
    }
    else if(command.substring(0,15)==="update/country/"){
        const countryId =Number(command.substring(15,command.lenght));
        if(isNaN(countryId))
        {
            console.log("Please insert the number after get/country/");
            command;
        }
        const country = countryRepository.getCountryById(countryId);
        if (!country) 
        {
            console.log(`Error: country with id ${countryId} not found.`);
            command;
        }
        else
        {
            country.nameCountry=readlineSync.question('Enter country: ');
            country.capital=readlineSync.question('Enter capital: ');
            country.population=Number(readlineSync.question('Enter population: '));
            while(isNaN(country.population)){
                console.log("please insert the number");
                country.population=Number(readlineSync.question('Enter population: '));
            }
            country.area=Number(readlineSync.question('Enter area: '));
            while(isNaN(country.area)){
                console.log("please insert the number");
                country.area=Number(readlineSync.question('Enter area: '));
            }
            let year=readlineSync.question('Enter year: ');
            let month=readlineSync.question('Enter month: ');
            let day=readlineSync.question('Enter day: ');
            country.yearOfFound=new Date(+year,+month,+day);
            if(countryRepository.updateCountry(country)){
                console.log("update");
                command;
            }
            else{
                command;}
        }
    }
    else if(command.substring(0,15)==="delete/country/"){
        const countryId =Number(command.substring(15,command.lenght));
        if(isNaN(countryId))
        {
            console.log("Please insert the number after delete/country/");
            command;
        }
        if(!countryRepository.deleteCountry(countryId)){
            console.log(`Error: country with id ${countryId} not found.`);
            command;
        }
        else{
            console.log(`delete country with id ${countryId}`);
            command;
        }

    }
    else if(command==="post/country"){
        let country=new Country;
        country.nameCountry=readlineSync.question('Enter country: ');
        country.capital=readlineSync.question('Enter capital: ');
        country.population=Number(readlineSync.question('Enter population: '));
        while(isNaN(country.population)){
            console.log("please insert the number");
            country.population=Number(readlineSync.question('Enter population: '));
        }
        country.area=Number(readlineSync.question('Enter area: '));
        while(isNaN(country.area)){
            console.log("please insert the number");
            country.area=Number(readlineSync.question('Enter area: '));
        }
        
        let year=readlineSync.question('Enter year: ');
        let month=readlineSync.question('Enter month: ');
        let day=readlineSync.question('Enter day: ');
        country.yearOfFound=new Date(+year,+month,+day);
        let id=countryRepository.addCountry(country);
        console.log(`id new country ${id}`);
    }
    else if(command==="end"){
        
        return ;
    }
    else {
        console.error(`Not supported command: '${command}'`);
    }
    //console.log("Enter your command: ");
    command;
}

