/**
 * @typedef Continent
 * @property {string} id.required
 * @property {string} nameContinent.required - name of the continent
 * @property {number} quantityOfCountries.required  
 * @property {string} ocean.required 
 * @property {number} numberOfTimeZone.required 
 * @property {number} area.required 
 */



class Continent {

    constructor(id, nameContinent, quantityOfCountries,ocean,area,numberOfTimeZone,avaUrl) {
        this.id = id;  // number
        this.nameContinent = nameContinent;  // string
        this.quantityOfCountries = quantityOfCountries;  // string
        this.ocean=ocean;
        this.area=area;
        this.numberOfTimeZone=numberOfTimeZone;
        this.avaUrl=avaUrl;
        
        // TODO: More fields
    }
 };
 
 module.exports = Continent;