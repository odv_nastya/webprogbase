/**
 * @typedef Country
 * @property {integer} id.required
 * @property {string} name.required - name of the country
 * @property {string} capital.required - capital 
 * @property {number} population.required 
 * @property {number} area.required 
 * @property {string} yearOfFound.required -year of foundation
 */



class Country {

    constructor(id, nameCountry, capital,population,area,yearOfFound,avaUrl,id_continent) {
        this.id = id;  // number
        this.nameCountry = nameCountry;  // string
        this.capital = capital;  // string
        this.population=population;
        this.area=area;
        this.yearOfFound=yearOfFound;
        this.id_continent=id_continent;
        this.avaUrl=avaUrl;
        
        // TODO: More fields
    }
 };
 
 module.exports = Country;