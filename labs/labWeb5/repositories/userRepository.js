// repositories/userRepository.js

const User = require('../models/user');
const JsonStorage = require('../jsonStorage');
const fs=require('fs').promises;
const mongoose = require('mongoose');
const userModel=require('../shema/user');
class UserRepository {
 
    constructor(dbUrl) {
        this.storage = new JsonStorage(dbUrl,userModel);
    }
 
    async getUsers() { 
        await this.storage.connect();
        const result=await this.storage.getItems();
        await this.storage.disconect();
        console.log(result);
        return result;
    }
 
    async getUserById(userId) {
        console.log(userId);
        await this.storage.connect();
        const result = await this.storage.getItembyId(userId);
        console.log(result);
        await this.storage.disconect();
        return result;
    }
 
    
};
module.exports = UserRepository;
