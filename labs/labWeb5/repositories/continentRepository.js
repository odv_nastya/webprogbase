const Continent = require('../models/continent');
const JsonStorage = require('../jsonStorage');
const e = require('express');
const fs=require('fs').promises;
const mongoose = require('mongoose');
const ContinentModel=require('../shema/continent');
class ContinentRepository {
 
    constructor(dbUrl) {
        this.storage = new JsonStorage(dbUrl,ContinentModel);
    }
 
    async getContinents() { 
        await this.storage.connect();
        const result=await this.storage.getItems();
        await this.storage.disconect();
        //console.log(result);
        return result;
    }
 
    async getContinentById(continentId) {
        console.log("TYTA");
        console.log(continentId);
        await this.storage.connect();
        const result = await this.storage.getItembyId(continentId);
        console.log(result);
        await this.storage.disconect();
        return result;
    }
    async addContinent(entityModel){
        await this.storage.connect();
        const id=await this.storage.addItem(entityModel);
        await this.storage.disconect();
        return id;
    }
    async updateContinent(entityModel){
        await this.storage.connect();
        await this.storage.updateItem(entityModel);
        await this.storage.disconect();
    }
    async deleteContinent(entityId){
        await this.storage.connect();
        const pic=await this.storage.getItembyId(entityId);
        await this.storage.deleteItem(entityId);
        await this.storage.disconect();
    }
 
    
};
module.exports = ContinentRepository;
 