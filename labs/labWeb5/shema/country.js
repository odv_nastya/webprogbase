const mongoose=require('mongoose');
const countrySchema = new mongoose.Schema({
    nameCountry: { type: String, required: true },
    capital: { type: String, required: true },
    population: { type: Number, required: true },
    area: { type: Number, required: true },
    avaUrl: { type: String, required: true },
    yearOfFound: { type: Date, required: true},
    id_continent: { type: mongoose.Schema.ObjectId, ref: 'Continent'},
    id_user: { type: mongoose.Schema.ObjectId, ref: 'User'}
 });
 const CountryModel = mongoose.model('Country', countrySchema);
 module.exports=CountryModel;
