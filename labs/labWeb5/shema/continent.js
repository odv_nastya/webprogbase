const mongoose=require('mongoose');
const continentSchema = new mongoose.Schema({
    nameContinent: { type: String, required: true },
    quantityOfCountries: { type: Number, required: true },
    ocean: { type: String, required: true },
    area: { type: Number, required: true },
    avaUrl: { type: String, required: true },
    numberOfTimeZone: { type: Number, required: true}
 });
 const ContinentModel=mongoose.model('Continent',continentSchema);
 module.exports=ContinentModel;