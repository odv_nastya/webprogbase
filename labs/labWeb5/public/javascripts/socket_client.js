const protocol = location.protocol === 'https:' ? 'wss:' : 'ws:';
const wsLocation = `${protocol}//${location.host}`;
const connection = new WebSocket(wsLocation)
console.log(`${protocol}//${location.host}`)
console.log("Web socket")
async function showToastMessage(messageText){
    console.log("In script")
    let toastEl = await fetch("/javascripts/templats/toast.mst")
    toastEl = await toastEl.text()
    
    item = messageText
    if (messageText.nameCountry){
        messageText.name = messageText.nameCountry
    }
    else{
        messageText.name = messageText.nameContinent
    }
    const renderedHtmlStr = Mustache.render(toastEl,{item})
    const toastOption = {
        animation: true,
        autohide:true,
        delay: 200000,
    };
    document.getElementById("cont").innerHTML=document.getElementById("cont").innerHTML+renderedHtmlStr
    console.log(messageText.id)
    $("#toast"+messageText.id).toast(toastOption);
    $("#toast"+messageText.id).toast('show');

}
connection.addEventListener('open', ()=>console.log("ws conected"))
connection.addEventListener('error', ()=>console.log("ws error"))
connection.addEventListener('message',
async (message) =>{
    await showToastMessage(JSON.parse(message.data))
    console.log(`ws message: ${JSON.parse(message.data)}`)
})
connection.addEventListener('close', ()=>console.log("ws disconnected"))