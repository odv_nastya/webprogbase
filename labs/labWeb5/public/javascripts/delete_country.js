

async function delFunction(){
    try{
        let loading= await fetch("/javascripts/templats/loading_but.mst")
        loading=await loading.text()
        document.getElementById("del").innerHTML=loading
        document.getElementById("del").setAttribute("disabled", true)
        let id =document.getElementById("countryId").value
        let response = await fetch(window.location.pathname,{
            method: "POST",
            params:{
                id: id
            }
        });
        console.log(response)
        response = await response.json()
        if(response.id===id){
            countries =JSON.parse(localStorage.getItem("countries2"))
            if(countries){
            for(let i=0; i<countries.length;i++){
                if(countries[i].id=== id){
                    countries.splice(i,1)
                    localStorage.setItem("countries2",JSON.stringify(countries))
                }
            }
        }
            let resp2=await fetch("/javascripts/templats/delete_country.html")
            resp2 =await resp2.text()
            document.getElementById("del-body").innerHTML=resp2
            $("#deleteModal").modal('hide')
        }
        else{
            alert("not delete")
        }
        
    }
    catch(err){
        alert(err)
    }
};