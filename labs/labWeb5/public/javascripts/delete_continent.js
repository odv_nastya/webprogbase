async function delFunction(){
    try{
        let loading= await fetch("/javascripts/templats/loading_but.mst")
        loading=await loading.text()
        document.getElementById("del").innerHTML=loading
        document.getElementById("del").setAttribute("disabled", true)
        let id =document.getElementById("continentId").value
        console.log("id")
        console.log(id)
        let response = await fetch(window.location.pathname,{
            method: "POST",
            params:{
                id: id
            }
        });
        console.log(response)
        response = await response.json()
        if(response.id===id){
            continents =JSON.parse(localStorage.getItem("countinent2"))
            if(continents){
            for(let i=0; i<continents.length;i++){
                if(continents[i].id=== id){
                    continents.splice(i,1)
                    localStorage.setItem("countinent2",JSON.stringify(continents))
                }
            }
            }
            let resp2=await fetch("/javascripts/templats/delete_continent.html")
            resp2 =await resp2.text()
            document.getElementById("del-body").innerHTML=resp2
            $("#deleteModal").modal('hide')
        }
        else{
            alert("not delete")
        }
        
    }
    catch(err){
        alert(err)
    }
};