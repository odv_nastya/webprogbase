current_page=0
name=""
pages_length=0
async function render(per_page){
    try{
        let element = document.getElementById("search-continent")
        element.setAttribute("disabled", true)
        document.getElementById('pag').innerHTML=""
        let inn=document.getElementById('cent').innerHTML
        let loading=await fetch("/javascripts/templats/loading.mst")
        loading= await loading.text()
        document.getElementById('cent').innerHTML=loading
        let resp = await fetch("/javascripts/templats/pagination.mst")
        resp = await resp.text()
        let response=await fetch(('/api/continent?'+"page="+current_page+"&per_page="+per_page+"&name="+name),{
            headers:{
                "OnPage":"true"
            }
        })
        response=await response.json()
        countries=response.continents
        let count = response.number
        count_countries=count
        pages_length= Math.ceil(count/5)
        console.log("len")
        console.log(pages_length)
        pages=[]
        for(let i=1;i<=pages_length;i++){
            console.log("in for")
            page ={}
            page.number=i
            page.is_active = ""
            pages.push(page)
        }
        console.log("current")
        console.log(current_page)
        pages[current_page-1].is_active="active"
        pre_dis=""
        ne_dis=""
        if(pages[0].is_active==="active") pre_dis="disabled"
        if(pages[pages_length-1].is_active==="active") ne_dis="disabled"
        let table = await fetch("/javascripts/templats/continents_table.mst")
        table =await table.text()
        document.getElementById("cent").innerHTML=inn
        const renderedHtmlStr = Mustache.render(resp,{pages, pre_dis, ne_dis})
        document.getElementById('pag').innerHTML=renderedHtmlStr
       
        console.log("Table")
        
        const renderedHtmlStr2 = Mustache.render(table,{countries})
        document.getElementById('continents-table').innerHTML = renderedHtmlStr2
        element.removeAttribute('disabled')
    }
    catch(err){
        console.log(err)
    }
}
window.addEventListener('load',async(le)=>{
    try{
        current_page=1
        await render(5)
    }
    catch(err){
        console.log(err)
    }
})
async function pageFunction(number){
    current_page=number
    await render(5)
}
async function prevFunction(){
    current_page-=1
    await render(5)
}
async function nextFunction(){
    current_page+=1
    await render(5)
}
document.getElementById('search-continent').addEventListener("change",async function(){
    current_page=1
    name=document.getElementById('search-continent').value
    await render(5)
})