// controllers/users.js
const CountryRepository = require('../repositories/countryRepository');
const UserRepository = require('../repositories/userRepository');
const ContinentRepository = require('../repositories/continentRepository');
const mediaRepository = require('../repositories/mediaRepository');
const reposit=require('../repositories/countryRepository')
const fs=require('fs').promises;
const fs2=require('fs');
const { strict } = require('assert');
const countryProperty = Symbol('country');
const mustache = require("mustache");
const config = require('./config');
 
 const { db: { host, port, name } } = config.dev;
 const connectionString = `mongodb+srv://${host}:${port}/${name}?retryWrites=true&w=majority`;
console.log(connectionString);

reposito=new CountryRepository(connectionString)
reposito2=new ContinentRepository(connectionString)
reposito3=new UserRepository(connectionString)
reposito4=new mediaRepository()

notifyAll=require("../bin/www")
module.exports = {
    async getCountries(req, res, next) {
        try{
            if (req.headers.onpage){
            console.log("onpage")
            const countries=await reposito.getCountries();
            let countries2= [];
            console.log("name")
            console.log(req.query.name);
            if(req.query.name){
                console.log("exist")
                for (let i=0; i<countries.length;i++)
            {
                if(countries[i].nameCountry.includes(req.query.name))
                {
                    console.log("We in if")
                    console.log(countries[i]);
                    countries2.push(countries[i]);
                }
                
            }
            }
            else countries2=countries;
            if(!req.query.page) req.query.page=1;
            let countries3=[];
            if(!('per_page' in req.query))req.query.per_page=1;
            if(req.query.next && (Math.ceil(countries2.length/req.query.per_page)!=req.query.page)) req.query.page++;
            if(req.query.prev&&(1!=req.query.page)) req.query.page--;
            
            if(req.query.per_page>100)req.query.per_page=100;
            const first = req.query.per_page *(req.query.page -1)
            const len = Math.min(countries2.length, req.query.page * req.query.per_page)
            
            for (let i =first; i<len; i++){
                countries2[i].id_continent.id=countries2[i].id_continent._id.toString()
                countries3.push(countries2[i])
            }
            Data={}
            Data.countries=countries3
            Data.number=countries2.length
            res.send(Data)
        }
        else{
            console.log(" not onpage")
            const countries= await reposito.getCountries()
            
                const result=await Promise.all([
                    fs.readFile("./views/partials/head.mst"),
                    fs.readFile("./views/partials/header.mst"),
                    fs.readFile("./views/partials/footer.mst"),
                    fs.readFile("./views/countries.mst")
                ])
                const head = result[0].toString();
                const header =result[1].toString();
                const footer =result[2].toString();
                const userTemplate =result[3].toString();
            const Data={
                count:countries.length
            }
            const partials = {
                head: head,
                header: header,
                footer: footer,
                
            };
            res.send(mustache.render(userTemplate,Data,partials))}
            
        }
        catch(err)
        {
            return await next(err);
        }   

    },

    async getCountryById(req, res, next) {
    try{
        const country =await reposito.getCountryById(req.params.id);
        if (country) {
            req[countryProperty] = country;
            const result=await Promise.all([
                fs.readFile("./views/partials/head.mst"),
                fs.readFile("./views/partials/header.mst"),
                 fs.readFile("./views/partials/footer.mst"),
                fs.readFile("./views/country.mst")
            ])
            const head =result[0].toString();
            const header = result[1].toString();
            const footer = result[2].toString();
            const userTemplate = result[3].toString();
           const Data={
             
           }
           const partials = {
            head: head,
            header: header,
            footer: footer,
            
          };
          Data.country=country;
          console.log(country)
          res.send(mustache.render(userTemplate,Data,partials))
            //res.send(req[countryProperty]);
        } else {
            res.sendStatus(404);
        }
    }
    catch(err)
    {
        return await next(err);
    }
    },
    async getCountryHandler(req, res, next) {
        try{
        const country =await reposito.getCountryById(req.params.id);
        if (country) {
            req[countryProperty] = country;
            next();
        } else {
            res.sendStatus(404);
        }
    }
    catch(err)
    {
        return await next(err);
    }

    },
    async deleteCountryById(req, res, next){
        try{
            console.log("ID")
            console.log(req.params.id)
        await reposito.deleteCountry(req.params.id);
        console.log("id2")
        console.log(req.params.id)
        id =req.params.id

        res.send({id})
        }
        catch(err){
            console.log(err)
            return await next(err);
        }
    },
    async addCountry(req,res,next){
        try{
        console.log("fghj");
        console.log(req.files);
        let country={};
        country.nameCountry=req.body.nameCountry;
        country.id_continent=req.body.continent;
        country.id_user=req.body.user;
        country.capital=req.body.capital;
        country.yearOfFound=req.body.datetime;
        country.population=req.body.population;
        country.area=req.body.area;
        console.log(req.files);
        const o = await reposito4.uploadRaw(req.files['avaUrl'].data);
        console.log(req.files)
        country.avaUrl=o.url;
        /*
        const jsonArray=JSON.parse(jsonText);*/
        if(!('nameCountry' in country)||!('capital'in country)||!('population' in country)||!('area' in country)||!('yearOfFound' in country)){
            res.sendStatus(400)
        }
        else{
            id=await reposito.addCountry(country);
            country.id=id;
            country.url='/api/country/'+id
            country.add_date=new Date(Date.now())
            req[countryProperty]=country
            console.log(req[countryProperty])
            notifyAll(country)
            res.send(req[countryProperty])
            // res.redirect('/api/country/'+id);
        }
    }
    catch(err)
    {
        return await next(err);
    }

    },
    async updateCountry(req,res,next){
        try{
        const country=await reposito.getCountryById(req.params.id);
        await fs.writeFileSync('./newCountry/country.json',req.files['country'].data);
        const jsonText=await fs.readFile('./newCountry/country.json');
        const jsonArray=JSON.parse(jsonText);
        if('nameCountry' in jsonArray) country.nameCountry=jsonArray.nameCountry;
        if('capital' in jsonArray) country.capital=jsonArray.capital;
        if('population' in jsonArray) country.population=jsonArray.population;
        if('area' in jsonArray) country.area=jsonArray.area;
        if('yearOfFound' in jsonArray) country.yearOfFound=jsonArray.yearOfFound;
        await reposito.updateCountry(country);
        req[countryProperty]=country;
        res.send(req[countryProperty])
        }
        catch(err)
        {
            return await next(err);
        }

    },
    async New(req,res,next)
    {
        try{
            const continents = await reposito2.getContinents();
            const users = await reposito3.getUsers();
        console.log(req,res)
        const result =await Promise.all([
            fs.readFile("./views/partials/head.mst"),
            fs.readFile("./views/partials/header.mst"),
            fs.readFile("./views/partials/footer.mst"),
            fs.readFile("./views/new.mst")
        ])
        const head = result[0].toString();
        const header = result[1].toString();
        const footer = result[2].toString();
        const userTemplate = result[3].toString();
       const Data={
         
       }
       Data.continents=continents;

       Data.users=users;
       console.log(continents);
       console.log(users);
       const partials = {
        head: head,
        header: header,
        footer: footer,
        
      };
      res.send(mustache.render(userTemplate,Data,partials))
    }
    catch(err){
        return await next(err);
    }
    },
    async anyErorr(err, req, res, next){
        console.error(err.stack)
        res.status(500).send('Something broke!') 
      }
};

