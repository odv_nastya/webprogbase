// controllers/users.js
const UserRepository = require('../repositories/userRepository');
//const reposit=require('../repositories/userRepository')
const fs = require("fs").promises;
const mustache = require("mustache");
const config = require('./config');
 
 const { db: { host, port, name } } = config.dev;
 const connectionString = `mongodb+srv://${host}:${port}/${name}?retryWrites=true&w=majority`;
console.log(connectionString);
const userProperty = Symbol('user');
reposit=new UserRepository(connectionString)
module.exports = {
    async getUsers(req, res, next) {
      try{
        console.log(req.query.page,req.query.per_page)
        if(!('per_page' in req.query))req.query.per_page=10
        if(req.query.per_page>100)req.query.per_page=100
        const users =await reposit.getUsers();
        if (users) {
            if(('page' in req.query)&&('per_page' in req.query))
            {
                const f=req.query.per_page*(req.query.page-1);
                console.log(f);
                let user=[];
                const lenght=Math.min(users.length,(req.query.page*req.query.per_page))
                console.log(lenght);
                for (let i=f;i<lenght;i++)
                {
                    user.push(users[i]);
                }
                req[userProperty] = user;
            }
            else req[userProperty] = users;
            const result =await Promise.all([
              fs.readFile("./views/partials/head.mst"),
              fs.readFile("./views/partials/header.mst"),

              fs.readFile("./views/partials/footer.mst"),
              fs.readFile("./views/users.mst")
            ])
            const head = result[0].toString();
            const header = result[1].toString();
            const footer = result[2].toString();
            const userTemplate = result[3].toString();
           const Data={}
           const partials = {
            head: head,
            header: header,
            footer: footer,
            
          };
          Data.users=users;
          res.send(mustache.render(userTemplate,Data,partials))
            //res.send(req[userProperty]);
        } else {
            res.sendStatus(404);
        }
      }
      catch(err)
      {
        return await next(err);
      }
    },

    async getUserById(req, res, next ) {
      try{
        const user =await reposit.getUserById(req.params.id);
        if (user) {
            req[userProperty] = user;
            const result=await Promise.all([
              fs.readFile("./views/partials/head.mst"),
              fs.readFile("./views/partials/header.mst"),
              fs.readFile("./views/partials/footer.mst"),
              fs.readFile("./views/user.mst")
            ])
            const head = result[0].toString();
            const header = result[1].toString();
            const footer = result[2].toString();
            const userTemplate = result[3].toString();
           const Data={
           }
           const partials = {
            head: head,
            header: header,
            footer: footer,
            
          };
          Data.user=user;
          res.send(mustache.render(userTemplate,Data,partials))
            //res.send(req[userProperty]);
        } else {
            res.sendStatus(404);
        }}
        catch(err)
        {
          return await next(err);
        }

    },
  async anyErorr(err, req, res, next){
    console.error(err.stack)
    res.status(500).send('Something broke!') 
  }

};

